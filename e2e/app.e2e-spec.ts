import { BluRayProjectPage } from './app.po';

describe('blu-ray-project App', () => {
  let page: BluRayProjectPage;

  beforeEach(() => {
    page = new BluRayProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
