import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataServiceService } from 'app/services/data-service.service';
import { GlobalVariables } from 'app/global/global_variables';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Movie } from 'app/interfaces/movie';
import { ImdbMovie } from 'app/interfaces/imdbMovie';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
// import {  } from 'events';

let timeout;

@Component({
  selector: 'app-seach-imdb',
  templateUrl: './seach-imdb.component.html',
  styleUrls: ['./seach-imdb.component.scss']
})
export class SeachImdbComponent implements OnInit {

  @Input() showImdbSearch: boolean;
  @Output() close: EventEmitter<any> = new EventEmitter();
  @Output() selectMovie: EventEmitter<any> = new EventEmitter();

  searchControl = new FormControl();
  imdbList = [];
  public imdbList$: Observable<ImdbMovie[]> = this.dataService.getImdbMovieList();

  constructor(public dataService: DataServiceService) { }

  ngOnInit() {
    timeout = setTimeout(() => { this.showImdbSearch = false; }, 200);

    this.searchControl.valueChanges.debounceTime(GlobalVariables.debounceTime).distinctUntilChanged().subscribe(keywords => {
      if (keywords) {
        this.dataService.searchImdbByName(keywords).then(result => {
          if (result.Response === 'True') {
            console.log(result);
            this.imdbList = result.Search;
          }
        });
      } else {
        this.imdbList = [];
      }
    });
  }

  closeModal() {
    this.searchControl.setValue('');
    this.showImdbSearch = false;
    clearTimeout(timeout);
    setTimeout(() => { this.close.emit(null); }, 700);
  }

  clearSearch() {
    this.searchControl.setValue('');
  }

  selectMovieToAdd(thismovie) {
    this.selectMovie.emit(thismovie);
  }
}
