import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeachImdbComponent } from './seach-imdb.component';

describe('SeachImdbComponent', () => {
  let component: SeachImdbComponent;
  let fixture: ComponentFixture<SeachImdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeachImdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeachImdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
