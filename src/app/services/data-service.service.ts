import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { GlobalVariables } from 'app/global/global_variables';
import { Movie } from 'app/interfaces/movie';
import { ImdbMovie } from 'app/interfaces/imdbMovie';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { SearchByKeywords } from 'app/interfaces/searchbykeywords';

import jsonfile from 'jsonfile';

@Injectable()
export class DataServiceService implements SearchByKeywords {

    public hasNext = false;
    public hasPrevious = false;
    private pageNumber = 1;
    private maximumPages = 0;
    public count = 0;
    public totalCount = 0;
    headers: Headers;
    options: RequestOptions;

    movieEmitter: Subject <Movie[]> = new Subject<Movie[]>();
    movieEmitter$: Observable <Movie[]> = this.movieEmitter.asObservable();

    imdbEmitter: Subject <ImdbMovie[]> = new Subject<ImdbMovie[]>();
    imdbEmitter$: Observable <ImdbMovie[]> = this.imdbEmitter.asObservable();

    constructor(public http: Http) {
        this.headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9' });
        this.options = new RequestOptions({ headers: this.headers });
    }

    searchbyKeywords(keyword: string) {
        this.getMovieList(undefined, keyword);
    }

    searchImdbByKeyword(keyword: string) {
        this.getImdbMovieList(keyword);
    }

    getMovieList(pageNumber = 1, queryString = ''): Observable<Movie[]> {
        let response: Response;
        this.pageNumber = pageNumber;

        this.http.get(`${GlobalVariables.url}?_limit=${GlobalVariables.pageSize}&_sort=title&_page=${this.pageNumber}&q=${queryString}`)
            .map(res => (response = res).json() as Movie[]).toPromise().then(data => {
                this.count = data.length;
                data.forEach(element => {
                    const poster = '';
                    this.getImdbData(element.imdbid).then(result => {
                        element.imdbMovie = result;
                    });
                });
                this.totalCount = Math.ceil(+response.headers.get('X-Total-Count'));
                // the number of pages based on the total number of products
                // returning from server and the global pageSize (number of items per page)
                this.maximumPages = Math.ceil(+response.headers.get('X-Total-Count') / GlobalVariables.pageSize);
                this.hasNext = this.hasNextItems();
                this.hasPrevious = this.hasPreviousItems();
                this.movieEmitter.next(data);
        });
        return this.movieEmitter$;
    }

    getImdbMovieList(queryString = ''): Observable<ImdbMovie[]> {
        let response: Response;
        const url = 'http://www.omdbapi.com/?s=' + queryString + '&plot=short&r=json&apikey=4ad6f7c3';
        if (queryString !== '') {

            this.http.get(url).map(res => (response = res).json() as ImdbMovie[])
                .toPromise()
                .then(data => {
                    this.imdbEmitter.next(data);
                })
                .catch(this.handleError);

            return this.imdbEmitter$;
        }
    }

    createNewMovie(title, imdbid, year, poster) {
        const movie = new Movie();
        movie.imdbid = imdbid;
        movie.title = title;
        movie.year = year;
        movie.poster = poster;

        const thisURL = `${GlobalVariables.url}`;
        const cpHeaders = new Headers({ 'Content-Type': 'application/json' });

        return this.http.post(thisURL, movie, {headers: cpHeaders});
    }

    deleteMovie(movieId) {
        const thisURL = `${GlobalVariables.url}`;
        const cpHeaders = new Headers({ 'Content-Type': 'application/json' });

        return this.http.delete(thisURL + '/' + movieId);
    }

    // IMDB Api - http://www.omdbapi.com/
    getImdbData(imdbId) {
        let response: Response;
        const url = 'http://www.omdbapi.com/?i=' + imdbId + '&plot=short&r=json&apikey=4ad6f7c3';
        if (imdbId !== '') {
            return this.http.get(url).map(res => (response = res).json())
                .toPromise()
                .then(this.extractMovieData)
                .catch(this.handleError);
        }
    }

    searchImdbByName(moviename) {
        let response: Response;
        const url = 'http://www.omdbapi.com/?s=' + moviename + '&plot=short&r=json&apikey=4ad6f7c3';
        if (moviename !== '') {
            return this.http.get(url).map(res => (response = res).json())
                .toPromise()
                .then(this.extractMovieData)
                .catch(this.handleError);
        }
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    private extractMovieList(res: ImdbMovie[]) {
        return res || [];
    }

    private extractMovieData(res: ImdbMovie) {
        return res || {};
    }

    private extractData(res: Response) {
        return res || {};
    }

    private hasNextItems(): boolean {
      if (this.pageNumber >= this.maximumPages) {
        return false;
      }
      return true;
    }

    private hasPreviousItems(): boolean {
      if (this.pageNumber <= 1) {
        return false;
      }
      return true;
    }

    public getNextItems() {
      if (this.hasNextItems()) {
        this.getMovieList(++this.pageNumber);
      }
    }

    public getPreviousItems() {
      if (this.hasPreviousItems()) {
        this.getMovieList(--this.pageNumber);
      }
    }
}
