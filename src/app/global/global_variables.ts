export class GlobalVariables {
    public static readonly url = 'http://localhost:3000/moviedb';
    public static readonly pageSize = 20;
    public static readonly debounceTime = 300;
    public static searchValue = '';
}
