import { Component, OnInit, Input, HostListener } from '@angular/core';
import { DataServiceService } from 'app/services/data-service.service';
import { FormControl } from '@angular/forms';
import { GlobalVariables } from 'app/global/global_variables';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Input() dataService: DataServiceService;
  searchControl = new FormControl();

  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.keyCode === 27 && this.searchControl.value !== '') {
      this.clearSearch();
    }
  }

  constructor() { }

  ngOnInit() {
    this.searchControl.valueChanges.debounceTime(GlobalVariables.debounceTime).distinctUntilChanged().subscribe(keywords => {
      GlobalVariables.searchValue = keywords;
      this.dataService.searchbyKeywords(keywords);
    });
  }

  clearSearch() {
    this.searchControl.setValue('');
    GlobalVariables.searchValue = '';
  }
}
