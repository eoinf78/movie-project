export interface IMovie {
    id: number;
    title: string;
    year: string;
    imdbid: string;
    poster: string;
    imdbMovie: Object;
}

export class Movie implements IMovie {
    id: number;
    title: string;
    year: string;
    imdbid: string;
    poster: string;
    imdbMovie: Object;
}
