export interface SearchByKeywords {
    searchbyKeywords(keyword: string);
    searchImdbByKeyword(keyword: string);
}
