export interface IImdbMovie {
    id: number;
    Title: string;
    Actors: number;
    Awards: string;
    Director: string;
    Metascore: string;
    DVD: string;
    Year: string;
    Poster: string;
    Rated: string;
}

export class ImdbMovie implements IImdbMovie {
    id: number;
    Title: string;
    Actors: number;
    Awards: string;
    Director: string;
    Metascore: string;
    DVD: string;
    Year: string;
    Poster: string;
    Rated: string;
}
