import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'movietitle'
})
export class MovietitlePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const returnVal = (value ? value : 'Serach Imdb');
    return returnVal;
  }

}
