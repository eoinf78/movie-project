import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { DataServiceService } from 'app/services/data-service.service';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { MovietitlePipe } from './pipes/movietitle.pipe';
import { SeachImdbComponent } from './seach-imdb/seach-imdb.component';
import { MovieListComponent } from './movie-list/movie-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    MovietitlePipe,
    SeachImdbComponent,
    MovieListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [ DataServiceService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
