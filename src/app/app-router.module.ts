import { MovieListComponent } from 'app/movie-list/movie-list.component';
import { RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full' },
    {path: 'home', component: MovieListComponent}
];

export const routerModule: ModuleWithProviders = RouterModule.forRoot(routes);
