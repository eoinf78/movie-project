import { Component, OnInit } from '@angular/core';
import { DataServiceService } from 'app/services/data-service.service';
import { Observable } from 'rxjs/Observable';
import { Movie } from 'app/interfaces/movie';
import { ImdbMovie } from 'app/interfaces/imdbMovie';
import { GlobalVariables } from 'app/global/global_variables';
import * as _ from 'lodash';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

  public movieList$: Observable<Movie[]> = this.dataService.getMovieList();
  public movieList;
  public showImdbSearch = false;

  title = 'My Movie Catalogue';
  showMovie = false;
  showListView = false;
  selectedMovie = new ImdbMovie();
  imdbMovieSearchResults = [];

  constructor(public dataService: DataServiceService) {
    this.movieList$.subscribe((res) => {
      this.movieList = res;
    });
  }

  ngOnInit() {
  }

  addImdbMovie(movie) {
    console.log(this.movieList);
    const existingMovie = _.filter(this.movieList, (m) => { return m.imdbid === movie.imdbID; }).length > 0;
    if (!existingMovie) {
      this.addNewMovie(movie.Title, movie.imdbID, movie.Year, movie.Poster);
    } else {
      alert('Looks like you already have this movie');
    }
  }

  addNewMovie(newTitle, newImdbIid, newYear, newPosterURL) {
    this.dataService.createNewMovie(newTitle, newImdbIid, newYear, newPosterURL).subscribe(
      res => {
        this.imdbMovieSearchResults = [];
        GlobalVariables.searchValue = '';
        this.dataService.searchbyKeywords('');
      },
      err => {
          console.log(err);
      }
    );
  }

  openImdbModal() {
    this.showImdbSearch = true;
  }

  onCloseDialog(): void {
    this.showImdbSearch = false;
  }

  toggleView() {
    this.showListView = !this.showListView;
  }

  openMovie(id, imdbData: ImdbMovie) {
    this.selectedMovie = new ImdbMovie();

    if (imdbData !== undefined) {
      this.selectedMovie = imdbData;
      this.selectedMovie.id = id;
    }
    this.showMovie = true;
  }

  removeMovie(id) {
    this.dataService.deleteMovie(id).subscribe(
      res => {
        this.closeModal();
        GlobalVariables.searchValue = '';
        this.dataService.searchbyKeywords('');
      },
      err => {
          console.log(err);
      }
    );
  }

  openSearch() {
    if (GlobalVariables.searchValue) {
      this.dataService.searchImdbByName(GlobalVariables.searchValue).then(result => {
          this.imdbMovieSearchResults = result.Search;
      });
    }

  }

  closeModal() {
    this.showMovie = false;
  }

}
